//
//  QNInternalConstants.h
//  Qonversion
//
//  Created by Surik Sarkisyan on 18.03.2021.
//  Copyright © 2021 Qonversion Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>

extern NSString *const kKeyQKeyChainUserID;
extern NSString *const kKeyQUserDefaultsOriginalUserID;
extern NSString *const kKeyQUserDefaultsUserID;
extern NSString *const kKeyQUserIDPrefix;
extern NSString *const kKeyQUserIDSeparator;
extern NSString *const kMainUserDefaultsSuiteName;
extern NSString *const kKeyQExperimentStartedEventName;
extern NSUInteger const kQPropertiesSendingPeriodInSeconds;
extern CGFloat const kJitter;
extern CGFloat const kFactor;
extern NSUInteger const kMaxDelay;
