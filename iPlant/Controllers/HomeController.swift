//
//  ViewController.swift
//  iPlant
//
//  Created by Pietro Messineo on 17.12.2021.
//

import UIKit
import CoreData
import RxCocoa
import RxSwift
import Lottie

var home = HomeController()
class HomeController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var plantImageView: UIImageView!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var animationView: AnimationView!
    
    var plants = [PlantResponse]()
    
    let disposeBag = DisposeBag()
    var plantIdsViewModel = PlantIdsViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        home = self
        
        getPlants()
        
        plantIdsViewModel.plantModel.plantErr.subscribe({ state in
            if let state = state.element {
                print("I GOT STATE \(state)")
                switch state {
                case .success:
                    //Stop previous animation
                    self.animationView.stop()
                    self.animationView.isHidden = true
                case .loading:
                    self.animationView.isHidden = false
                    self.startAnimationScan()
                    self.updateView(shouldHide: true)
                case .error:
                    //Stop previous animation
                    self.animationView.stop()
                    self.animationView.isHidden = true
                    self.updateView(shouldHide: false)
                case .waiting:
                    self.animationView.isHidden = true
                }
            }
        }).disposed(by: disposeBag)
        
        plantIdsViewModel.plantModel.plantInfo.subscribe({ data in
            if let scannedPlants = data.element, scannedPlants?.isEmpty == false {
                //Refresh plants array
                self.plants.removeAll()
                //Reload planys array
                self.plants = scannedPlants ?? [PlantResponse]()
                //Remove labels
                if self.plants.isEmpty {
                    self.updateView(shouldHide: false)
                }else {
                    self.updateView(shouldHide: true)
                }
                
                self.collectionView.reloadData()
            }
        }).disposed(by: disposeBag)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return plants.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "plantCell", for: indexPath) as! PlantCollectionViewCell
        cell.configure(plant: plants[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //Present Plan detail - Pass Response to detail
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "detailVc") as! PlantDetailController
        vc.plantResponse = plants[indexPath.item]
        self.present(vc, animated: true, completion: nil)
    }
    
    func getPlants() {
        //Remove all previous scanned elements
        self.plants.removeAll()
        //Access to AppDelegate for grab reference to CoreData
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Plants")
        do {
            let result = try managedContext.fetch(fetchRequest)
            print("I GOT RESULT \(result)")
            var plantsIds = [Int]()
            for data in result {
                plantsIds.append(Int(data.value(forKey: "plantId") as! Int64))
            }
            //Fetch plants from cloud cache
            if !plantsIds.isEmpty {
                plantIdsViewModel.dataFetch(ids: plantsIds)
            }
        }catch {
            print("ERROR FETCHING CACHE")
        }
    }
    
    func startAnimationScan() {
        animationView.isHidden = false
        
        // 1. Set animation content mode
        animationView.contentMode = .scaleAspectFit
        
        // 2. Set animation loop mode
        animationView.loopMode = .loop
        
        // 3. Adjust animation speed
        animationView.animationSpeed = 1
        
        // 4. Play animation
        animationView.play()
    }
    
    func updateView(shouldHide: Bool) {
        self.plantImageView.isHidden = shouldHide
        self.messageLabel.isHidden = shouldHide
    }

}

