//
//  InAppController.swift
//  iPlant
//
//  Created by Pietro Messineo on 20.12.2021.
//

import UIKit
import Qonversion
import SafariServices
import FirebaseRemoteConfig

class InAppController: UIViewController {

    @IBOutlet weak var subscribeButton: UIButton!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var trialLabel: UILabel!
    @IBOutlet weak var offerLabel: UILabel!
    
    @IBOutlet weak var firstFeatureLabel: UILabel!
    @IBOutlet weak var secondFeatureLabel: UILabel!
    @IBOutlet weak var thirdFeatureLabel: UILabel!
    @IBOutlet weak var bigTitleLabel: UILabel!
    
    private var remoteConfig : RemoteConfig!
    
    var iap = [String: Qonversion.Product]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        subscribeButton.layer.cornerRadius = 20
        
        fetchRemoteConfig()

        IAPManager.shared.getIapList(completion: { iapData in
            print("I GOT IAP DATA \(iapData)")
            self.iap = iapData
            
            //We got IAP - Populate the screen
            let offer = iapData["weekly_7days_trial"]
            //Setup trial label
            if let freeTrialDays = offer?.skProduct?.subscriptionPeriod?.numberOfUnits {
                self.trialLabel.text = "Start your \(freeTrialDays)-days FREE trial"
            }else {
                self.trialLabel.text = ""
            }
            
            //Setup offer label
            let price = String(format: "%.2f", ((offer?.skProduct?.price.doubleValue ?? 0)))
            self.offerLabel.text = "Then \(Currency.currency(for: offer?.skProduct?.prettyCurrency ?? "USD")!.shortestSymbol)\(price) weekly"
            
        })
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print(#function)
        fetchData()
    }
    
    func fetchRemoteConfig() {
        remoteConfig = RemoteConfig.remoteConfig()
        let settings = RemoteConfigSettings()
        settings.minimumFetchInterval = 0
        remoteConfig.configSettings = settings
    }
    
    func fetchData() {
        
        remoteConfig.fetch { (status, error) in
            
            if status == .success {
                print("Config fetched!")
                self.remoteConfig.activate() { (success,error) in
                    //Remote config activation state
                    print("Success \(success) AND ERROR \(error)")
                }
            } else {
                print("Config not fetched")
                print("Error: \(error?.localizedDescription ?? "No error available.")")
            }
            self.remoteCheckAction()
        }
    }
    
    func remoteCheckAction() {
        print("OBTAINED STATE OF REMOTE CONFIG \(remoteConfig["shouldHideCloseButton"].boolValue)")
        let shouldHideCloseButton = remoteConfig["shouldHideCloseButton"].boolValue
        closeButton.isHidden = shouldHideCloseButton
        
#if DEBUG
        closeButton.isHidden = false
#endif
        
        //String to JSON
        do {
            guard let data = remoteConfig["iap_config"].stringValue?.data(using: .utf8) else {
                return
            }
            
            let jsonData = try JSONDecoder().decode(InAppResponse.self, from: data)
            //Populate the bullet points
            if let bigTitle = jsonData.big_centered_top_title, bigTitle != "" {
                bigTitleLabel.text = bigTitle
            }
            
            if let firstFeature = jsonData.first_feature, firstFeature != "" {
                firstFeatureLabel.text = "• \(firstFeature)"
            }
            
            if let secondFeature = jsonData.second_feature, secondFeature != "" {
                secondFeatureLabel.text = "• \(secondFeature)"
            }
            
            if let thirdFeature = jsonData.third_feature, thirdFeature != "" {
                thirdFeatureLabel.text = "• \(thirdFeature)"
            }
            
            if let buttonTitle = jsonData.green_button_text, buttonTitle != "" {
                subscribeButton.setTitle(buttonTitle, for: .normal)
            }
        }
        catch {
            print("ERROR DECODING")
        }
    }

    @IBAction func closeAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func subscribeAction(_ sender: Any) {
        if let offer = iap["weekly_7days_trial"] {
            IAPManager.shared.purchaseProduct(product: offer, completion: { success in
                if success {
                    self.dismiss(animated: true, completion: nil)
                }else {
                    self.showAlertWithTitleAndMessage(title: "Oh oh", message: "Looks like something went wrong.")
                }
            })
        }
    }
    
    @IBAction func restorePurchase(_ sender: Any) {
        IAPManager.shared.restorePurchases(completion: { success in
            if success {
                //Show in-app restored
                self.showAlertWithTitleAndMessage(title: "Done ✅", message: "Purchase restored correctly. ")
            }else {
                //Show in-app error
                self.showAlertWithTitleAndMessage(title: "Oh oh", message: "No purchase found.")
            }
        })
    }
    
    @IBAction func termsAction(_ sender: Any) {
        //Open Terms on Safari
        openLink(link: "https://www.lpcloudapps.com/apps-tos")
    }
    
    @IBAction func privacyAction(_ sender: Any) {
        //Open Privacy on Safari
        openLink(link: "https://www.lpcloudapps.com/apps-privacy-policy")
    }
}

