//
//  PlantDetailController.swift
//  iPlant
//
//  Created by Pietro Messineo on 17.12.2021.
//

import UIKit
import ZKCarousel
import Kingfisher

class PlantDetailController: UIViewController {
    
    @IBOutlet weak var scrollableView: UIView!
    @IBOutlet weak var contentDetailView: UIView!
    //Detail
    @IBOutlet weak var plantNameLabel: UILabel!
    @IBOutlet weak var structuredPlantNameLabel: UILabel!
    @IBOutlet weak var wikiDescription: UITextView!
    @IBOutlet weak var classLabel: UILabel!
    @IBOutlet weak var familyLabel: UILabel!
    @IBOutlet weak var genusLabel: UILabel!
    @IBOutlet weak var orderLabel: UILabel!
    @IBOutlet weak var phylumLabel: UILabel!
    @IBOutlet weak var healthView: UIView!
    @IBOutlet weak var diseaseView: UIView!
    @IBOutlet weak var healthProbabilityLabel: UILabel!
    @IBOutlet weak var diseaseProbabilityLabel: UILabel!
    @IBOutlet weak var diseaseCauseLabel: UILabel!
    @IBOutlet weak var diseaseDescription: UITextView!
    
    @IBOutlet weak var backgroundRectangleOrder: UIView!
    @IBOutlet weak var backgroundRectangleClass: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var alternativeNamesCollectionView: UICollectionView!
    
    @IBOutlet weak var containerFullScreenImageView: UIView!
    @IBOutlet weak var fullScreenImageView: UIImageView!
    
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var diseaseDescriptionHeightConstraint: NSLayoutConstraint!
    
    var plantResponse: PlantResponse!
    
    var rectBackgroundLayer   = CAShapeLayer()
    var rectOverlayLayer   = CAShapeLayer()
    
    var rect2BackgroundLayer   = CAShapeLayer()
    var rect2OverlayLayer   = CAShapeLayer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        contentDetailView.layer.cornerRadius = 15
        backgroundRectangleClass.layer.cornerRadius = 8
        backgroundRectangleOrder.layer.cornerRadius = 8
        
        //Dynamically set description height
        DispatchQueue.main.asyncAfter(deadline: .now()+0.1, execute: {
            let textViewHeight = self.plantResponse.suggestions?.first?.plantDetails.wikiDescription?.value.height(withConstrainedWidth: self.wikiDescription.frame.width, font: .systemFont(ofSize: 14)) ?? 0.0
            self.heightConstraint.constant = textViewHeight + 40
        })
        
        plantNameLabel.text = plantResponse.suggestions?.first?.plantName
        structuredPlantNameLabel.text = (plantResponse.suggestions?.first?.plantDetails.structuredName.genus ?? "").capitalized + " " + (plantResponse.suggestions?.first?.plantDetails.structuredName.species ?? "").capitalized
        wikiDescription.text = plantResponse.suggestions?.first?.plantDetails.wikiDescription?.value
        
        classLabel.text = plantResponse.suggestions?.first?.plantDetails.taxonomy.taxonomyClass
        familyLabel.text = plantResponse.suggestions?.first?.plantDetails.taxonomy.family
        genusLabel.text = plantResponse.suggestions?.first?.plantDetails.taxonomy.genus
        orderLabel.text = plantResponse.suggestions?.first?.plantDetails.taxonomy.order
        phylumLabel.text = plantResponse.suggestions?.first?.plantDetails.taxonomy.phylum
        
        print("I GOT PLANT RESPONSE", plantResponse)
        setupHealthCircle(healthProbability: plantResponse.health_assessment?.is_healthy_probability ?? 0.0, customView: healthView, firstShape: rectBackgroundLayer, secondShape: rectOverlayLayer)
        
        //Setup disease information
        DispatchQueue.main.asyncAfter(deadline: .now()+0.1, execute: {
            let diseasesTextViewHeight = self.plantResponse.health_assessment?.diseases?.first?.disease_details?.description?.height(withConstrainedWidth: self.diseaseDescription.frame.width, font: .systemFont(ofSize: 14)) ?? 0.0
            self.diseaseDescriptionHeightConstraint.constant = diseasesTextViewHeight + 24
        })
        diseaseDescription.text = self.plantResponse.health_assessment?.diseases?.first?.disease_details?.description
        
        diseaseCauseLabel.text = self.plantResponse.health_assessment?.diseases?.first?.name
        
        //diseaseProbabilityLabel.text = "\(Int(((self.plantResponse.health_assessment?.diseases?.first?.probability ?? 0.0) * 100).rounded()))%"
        
        setupHealthCircle(healthProbability: self.plantResponse.health_assessment?.diseases?.first?.probability ?? 0.0, customView: diseaseView, firstShape: rect2BackgroundLayer, secondShape: rect2OverlayLayer)
        
    }
    
    func setupHealthCircle(healthProbability: Double, customView: UIView, firstShape: CAShapeLayer, secondShape: CAShapeLayer) {
        DispatchQueue.main.asyncAfter(deadline: .now()+0.2, execute: { [self] in
            let circlePath = UIBezierPath(roundedRect: CGRect(x: 0, y: 0, width: customView.frame.size.width, height: 28), byRoundingCorners: .allCorners, cornerRadii: CGSize(width: 8.0, height: 0.0))
            
            print("I OBTAINED HEALTH PROBABILITY \(healthProbability)")
            let alternativeCirclePath = UIBezierPath(roundedRect: CGRect(x: 0, y: 0, width: customView.frame.size.width * healthProbability, height: 28), byRoundingCorners: .allCorners, cornerRadii: CGSize(width: 8.0, height: 0.0))
            
            firstShape.path = circlePath.cgPath
            firstShape.fillColor = UIColor(named: "BackgroundLightGreen")?.cgColor
            customView.layer.addSublayer(firstShape)
            
            secondShape.path = alternativeCirclePath.cgPath
            
            //Setup GUI for health
            let healthPercentage = Int((healthProbability * 100).rounded())
            
            if healthPercentage >= 0 && healthPercentage <= 20 {
                //Awful
                if customView == healthView {
                    healthProbabilityLabel.text = "Awful \(healthPercentage)%"
                }else {
                    diseaseProbabilityLabel.text = "\(healthPercentage)%"
                }
                secondShape.fillColor = UIColor(named: "Awful")?.cgColor
            }else if healthPercentage >= 20 && healthPercentage <= 50 {
                //Bad
                if customView == healthView {
                    healthProbabilityLabel.text = "Bad \(healthPercentage)%"
                }else {
                    diseaseProbabilityLabel.text = "\(healthPercentage)%"
                }
                secondShape.fillColor = UIColor(named: "Bad")?.cgColor
            }else if healthPercentage >= 50 && healthPercentage <= 70 {
                //Good
                if customView == healthView {
                    healthProbabilityLabel.text = "Good \(healthPercentage)%"
                }else {
                    diseaseProbabilityLabel.text = "\(healthPercentage)%"
                }
                secondShape.fillColor = UIColor(named: "Good")?.cgColor
            }else if healthPercentage >= 70 && healthPercentage <= 100 {
                //Excellent
                if customView == healthView {
                    healthProbabilityLabel.text = "Excellent \(healthPercentage)%"
                }else {
                    diseaseProbabilityLabel.text = "\(healthPercentage)%"
                }
                secondShape.fillColor = UIColor(named: "AccentColor")?.cgColor
            }
            
            customView.layer.addSublayer(secondShape)
            customView.bringSubviewToFront(customView == healthView ? healthProbabilityLabel : diseaseProbabilityLabel)
            
        })
    }
    
    @IBAction func closeFullScreenView(_ sender: Any) {
        self.containerFullScreenImageView.isHidden = true
    }
    

}

extension PlantDetailController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == self.collectionView {
            if let images = plantResponse.suggestions?.first?.plantDetails.wikiImages {
                return images.count
            }else {
                return 0
            }
        }else {
            return plantResponse.suggestions?.first?.plantDetails.commonNames?.count ?? 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.collectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "carouselCell", for: indexPath) as! CarouselCollectionViewCell
            if let images = plantResponse.suggestions?.first?.plantDetails.wikiImages {
                cell.configure(image: images[indexPath.row].value)
            }
            return cell
        }else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "altnameCell", for: indexPath) as! AltNamesCollectionViewCell
            cell.configure(name: plantResponse.suggestions?.first?.plantDetails.commonNames?[indexPath.row] ?? "")
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.collectionView {
            //Assign image to full screen
            if let images = plantResponse.suggestions?.first?.plantDetails.wikiImages {
                self.fullScreenImageView.kf.setImage(with: URL(string: images[indexPath.row].value))
            }
            //Show container view
            self.containerFullScreenImageView.isHidden = false
        }
        
    }
    
}

extension UIViewController {
    func downloadImage(with urlString : String , imageCompletionHandler: @escaping (UIImage?) -> Void){
        guard let url = URL.init(string: urlString) else {
            return  imageCompletionHandler(nil)
        }
        let resource = ImageResource(downloadURL: url)
        
        KingfisherManager.shared.retrieveImage(with: resource, options: nil, progressBlock: nil) { result in
            switch result {
            case .success(let value):
                imageCompletionHandler(value.image)
            case .failure:
                imageCompletionHandler(nil)
            }
        }
    }
}

extension String {
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
    
        return ceil(boundingBox.height)
    }
}
