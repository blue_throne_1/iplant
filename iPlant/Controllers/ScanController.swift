//
//  ScanControllerViewController.swift
//  iPlant
//
//  Created by Pietro Messineo on 17.12.2021.
//

import UIKit
import Lottie
import RxSwift
import RxCocoa
import CoreData

var scanController = ScanController()
class ScanController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {

    @IBOutlet weak var scannedImageView: UIImageView!
    @IBOutlet weak var animationView: AnimationView!
    
    private var doneAnimationView: AnimationView?
    
    let plantInfoViewModel = PlantInfoViewModel()
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        scanController = self
        
        animationView.isHidden = true
        doneAnimationView?.isHidden = true
        
        scannedImageView.layer.cornerRadius = 20
        
        plantInfoViewModel.plantModel.plantErr.subscribe({ state in
            if let state = state.element {
                switch state {
                case .error:
                    DispatchQueue.main.async {
                        self.showAlertWithTitleAndMessage(title: "Oh oh", message: "Looks like that something went wrong. Try to take another picture.")
                    }
                default:
                    break
                }
            }
        }).disposed(by: disposeBag)
        
        plantInfoViewModel.plantModel.plantInfo.subscribe({ data in
            if let plantData = data.element, let plantResponse = plantData {
                //Obtained data
                self.doneAnimation()
                self.saveToCache(plant: plantResponse)
                //After 1s show the detail
                DispatchQueue.main.asyncAfter(deadline: .now()+1, execute: {
                    self.doneAnimationView?.removeFromSuperview()
                    //Present Plan detail - Pass Response to detail
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "detailVc") as! PlantDetailController
                    vc.plantResponse = plantResponse
                    self.present(vc, animated: true, completion: nil)
                })
            }
        }).disposed(by: disposeBag)
        
    }
    
    func openCamera() {
#if targetEnvironment(simulator)
        scannedImageView.image = UIImage(named: "ananas")
        plantInfoViewModel.dataFetch(plantImage: UIImage(named: "ananas")!)
        startAnimationScan()
#else
        let vc = UIImagePickerController()
        vc.sourceType = .camera
        vc.allowsEditing = false
        vc.delegate = self
        present(vc, animated: true)
#endif
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)
        
        guard let image = info[.originalImage] as? UIImage else {
            print("No image found")
            return
        }
        
        scannedImageView.image = image.resizedTo1MB()
        
        //Call API AND SHOW SOME SCAN ANIMATION
        plantInfoViewModel.dataFetch(plantImage: image.resizedTo1MB()!)
        startAnimationScan()
    }
    
    func startAnimationScan() {
        animationView.isHidden = false
        
        // 1. Set animation content mode
        animationView.contentMode = .scaleAspectFit
        
        // 2. Set animation loop mode
        animationView.loopMode = .loop
        
        // 3. Adjust animation speed
        animationView.animationSpeed = 0.5
        
        // 4. Play animation
        animationView.play()
    }
    
    func doneAnimation() {
        //Stop previous animation
        animationView.stop()
        animationView.isHidden = true
        
        doneAnimationView?.isHidden = false
        // 2. Start AnimationView with animation name (without extension)
        
        doneAnimationView = .init(name: "done")
        
        doneAnimationView!.frame = scannedImageView.frame
        
        // 3. Set animation content mode
        doneAnimationView!.contentMode = .scaleAspectFit
        
        // 4. Set animation loop mode
        doneAnimationView!.loopMode = .playOnce
        
        // 5. Adjust animation speed
        doneAnimationView!.animationSpeed = 1
        
        view.addSubview(doneAnimationView!)
        
        // 6. Play animation
        doneAnimationView!.play()
    }
    
    func saveToCache(plant: PlantResponse) {
        //Access to AppDelegate for grab reference to CoreData
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let managedContext = appDelegate.persistentContainer.viewContext
        //Create a Docs+CoreData object
        let plants = Plants(context: managedContext)
        plants.plantId = Int64(plant.id)
        
        do {
          try managedContext.save()
            home.getPlants()
        } catch let error as NSError {
          print("Could not save. \(error), \(error.userInfo)")
        }
    }

}

extension UIImage {

    func resized(withPercentage percentage: CGFloat) -> UIImage? {
        let canvasSize = CGSize(width: size.width * percentage, height: size.height * percentage)
        UIGraphicsBeginImageContextWithOptions(canvasSize, false, scale)
        defer { UIGraphicsEndImageContext() }
        draw(in: CGRect(origin: .zero, size: canvasSize))
        return UIGraphicsGetImageFromCurrentImageContext()
    }

    func resizedTo1MB() -> UIImage? {
        guard let imageData = self.pngData() else { return nil }
        let megaByte = 1000.0

        var resizingImage = self
        var imageSizeKB = Double(imageData.count) / megaByte // ! Or devide for 1024 if you need KB but not kB

        while imageSizeKB > megaByte { // ! Or use 1024 if you need KB but not kB
            guard let resizedImage = resizingImage.resized(withPercentage: 0.5),
            let imageData = resizedImage.pngData() else { return nil }

            resizingImage = resizedImage
            imageSizeKB = Double(imageData.count) / megaByte // ! Or devide for 1024 if you need KB but not kB
        }

        return resizingImage
    }
}
