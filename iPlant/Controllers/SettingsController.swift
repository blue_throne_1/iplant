//
//  SettingsController.swift
//  iPlant
//
//  Created by Pietro Messineo on 17.12.2021.
//

import UIKit
import CoreData
import MessageUI

class SettingsController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIPickerViewDelegate, UIPickerViewDataSource, MFMailComposeViewControllerDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var customPickerView: UIView!
    @IBOutlet weak var languageButton: UIButton!
    @IBOutlet weak var languagePickerView: UIPickerView!
    
    var settings = ["Language", "Clear cache", "QON ID", "Terms & Conditions", "Privacy Policy", "Contact us"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        languagePickerView.delegate = self
        languagePickerView.dataSource = self
        
        languageButton.layer.cornerRadius = languageButton.frame.height / 2
        
        //print("LIST OF LANGUAGES \(Locale.isoLanguageCodes)")
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }else if section == 1 {
            return 2
        }else {
            return 3
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "settingCell") as! SettingTableViewCell
        if indexPath.section == 0 {
            cell.configure(setting: settings[0])
        }else if indexPath.section == 1 {
            cell.configure(setting: settings[indexPath.row + 1])
        }else {
            cell.configure(setting: settings[indexPath.row + 3])
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            //Show language picker
            customPickerView.isHidden = false
        }else if indexPath.section == 1 {
            if indexPath.row == 0 {
                //Clear cache - Alert
                let alert = UIAlertController(title: "Empty Cache", message: "Are you sure you want to delete all your saved scans?", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Yes", style: .destructive, handler: { _ in
                    self.clearCoreData()
                }))
                
                alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
                
                self.present(alert, animated: true, completion: nil)
            }else if indexPath.row == 1 {
                self.showQonId()
            }
        }else if indexPath.section == 2 {
            if indexPath.row == 0 {
                self.openLink(link: "https://www.lpcloudapps.com/apps-tos")
            }else if indexPath.row == 1 {
                self.openLink(link: "https://www.lpcloudapps.com/apps-privacy-policy")
            }else if indexPath.row == 2 {
                //OPEN MAIL COMPOSER
                self.configureMail()
            }
        }
    }
    
    func configureMail() {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(["info@lpcloudapps.com"])
            mail.setSubject("Plant ID Support")
            mail.setMessageBody("<p>Hi Team 👋🏻 I have an issue with the app: </p>", isHTML: true)
            present(mail, animated: true)
        } else {
            // show failure alert
            showAlertWithTitleAndMessage(title: "Oh oh!", message: "Impossible to send the email. You can manually send us an email at: info@lpcloudapps.com")
        }
    }
    
    func clearCoreData() {
        //Access to AppDelegate for grab reference to CoreData
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Plants")
        do {
            let result = try managedContext.fetch(fetchRequest)
            for data in result {
                managedContext.delete(data)
            }
            
            do {
                try managedContext.save()
                home.getPlants()
                print("Cleared all data!")
            } catch let error as NSError  {
                print("Could not save \(error), \(error.userInfo)")
            } catch {
                
            }
            
        }catch {
            print("ERROR FETCHING CACHE")
        }
    }
    
    func showQonId() {
        let alert = UIAlertController(title: "Your Qon ID", message: UserDefaults.standard.string(forKey: "qonversionUid"), preferredStyle: .alert)
        let action = UIAlertAction(title: "Copy ID", style: .cancel, handler: { _ in
            //Save ID into clipboard
            let pasteboard = UIPasteboard.general
            pasteboard.string = UserDefaults.standard.string(forKey: "qonversionUid")
        })
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return Locale.isoLanguageCodes.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let language = LanguageHelper.getLanguageStringFrom(code: Locale.isoLanguageCodes[row])
        return language
    }
    
    var currentIndex = 0
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        currentIndex = row
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    
    @IBAction func languageAction(_ sender: Any) {
        customPickerView.isHidden = true
        UserDefaults.standard.set(Locale.isoLanguageCodes[currentIndex], forKey: "languageIso")
        self.tableView.reloadData()
    }
    
}

struct LanguageHelper {
    static func getLanguageStringFrom(code:String) -> String? {
        let locale: NSLocale? = NSLocale(localeIdentifier: "en")
        return locale?.displayName(forKey: .identifier, value: code)
    }
}
