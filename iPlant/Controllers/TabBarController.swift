//
//  TabBarController.swift
//  iPlant
//
//  Created by Pietro Messineo on 17.12.2021.
//

import UIKit

class TabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        if #available(iOS 15, *) {
            let appearance = UITabBarAppearance()
            appearance.configureWithOpaqueBackground()
            appearance.backgroundColor = .systemBackground
            UITabBar.appearance().standardAppearance = appearance
            UITabBar.appearance().scrollEdgeAppearance = appearance
        }
        
        //Push IAP Screen - inAppVc
        DispatchQueue.main.asyncAfter(deadline: .now()+0.3, execute: {
            if !UserDefaults.standard.bool(forKey: "isProNew") {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "inAppVc") as! InAppController
                vc.modalPresentationStyle = .overFullScreen
                self.present(vc, animated: true, completion: nil)
            }
        })
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        if tabBar.selectedItem?.title == "Scan" {
            //Open camera - For Plant Scan
            DispatchQueue.main.asyncAfter(deadline: .now()+0.4, execute: {
                scanController.openCamera()
            })
        }
    }

}
