//
//  Extensions.swift
//  iPlant
//
//  Created by Pietro Messineo on 20.12.2021.
//

import Foundation
import UIKit
import SafariServices

extension UIViewController {
    func showAlertWithTitleAndMessage(title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))

        self.present(alert, animated: true)
    }
    
    func openLink(link: String) {
        if let url = URL (string: link) {
            let controller = SFSafariViewController(url: url)
            self.present(controller, animated: true, completion: nil)
        }
    }
}
