//
//  IAPHelper.swift
//  iPlant
//
//  Created by Pietro Messineo on 20.12.2021.
//

import Foundation
import Qonversion

class IAPManager {
    static let shared = IAPManager()
    let userDefault = UserDefaults.standard
    
    private init() { }
    
    func getIapList(completion: @escaping ([String : Qonversion.Product]) -> ()) {
        Qonversion.products({ productsList, error in
            let product = productsList["Subscription"]
            print("I GOT PRODUCTS \(product) AND ALL PRODUCTS \(productsList)")
            completion(productsList)
        })
    }
    
    func purchaseProduct(product: Qonversion.Product, completion: @escaping (Bool) -> ()) {
        print("Purchase product \(product.qonversionID)")
        Qonversion.purchaseProduct(product, completion: { (permissions, error, isCancelled) in
            //TODO: DOUBLE CHECK THIS
            print("I GOT PERMISSIONS \(permissions) AND ERROR \(error) AND IS CANCELLED \(isCancelled)")
            if let premium: Qonversion.Permission = permissions["Subscription"], premium.isActive {
                // Flow for success state
                self.userDefault.set(true, forKey: "isProNew")
                completion(true)
            }else {
                completion(false)
            }
        })
    }
    
    func checkUserPermissions(completion: @escaping (Bool) -> ()) {
        Qonversion.checkPermissions { (permissions, error) in
            if let error = error {
                // handle error
                return
            }
            
            print("I GOT PERMISSIONS \(permissions)")
            
            if let premium: Qonversion.Permission = permissions["Subscription"], premium.isActive {
                switch premium.renewState {
                case .willRenew, .nonRenewable:
                    // .willRenew is the state of an auto-renewable subscription
                    // .nonRenewable is the state of consumable/non-consumable IAPs that could unlock lifetime access
                    break
                case .billingIssue:
                    // Grace period: permission is active, but there was some billing issue.
                    // Prompt the user to update the payment method.
                    break
                case .cancelled:
                    // The user has turned off auto-renewal for the subscription, but the subscription has not expired yet.
                    // Prompt the user to resubscribe with a special offer.
                    break
                default: break
                }
                
                self.userDefault.set(true, forKey: "isProNew")
                completion(true)
            }else {
                self.userDefault.set(false, forKey: "isProNew")
                completion(false)
            }
        }
    }
    
    func restorePurchases(completion: @escaping (Bool) -> ()) {
        Qonversion.restore { [weak self] (permissions, error) in
            if let error = error {
                // Handle error
            }
            
            //TODO: CHECK PERMISSIONS
            print("I GOT PERMISSIONS \(permissions)")
            if let permission: Qonversion.Permission = permissions["Subscription"], permission.isActive {
                // Restored and permission is active
                self?.userDefault.set(true, forKey: "isProNew")
                completion(true)
            }else {
                self?.userDefault.set(false, forKey: "isProNew")
                completion(false)
            }
        }
    }
}

struct Currency {
   
   /// Returns the currency code. For example USD or EUD
   let code: String
   
   /// Returns currency symbols. For example ["USD", "US$", "$"] for USD, ["RUB", "₽"] for RUB or ["₴", "UAH"] for UAH
   let symbols: [String]
   
   /// Returns shortest currency symbols. For example "$" for USD or "₽" for RUB
   var shortestSymbol: String {
      return symbols.min { $0.count < $1.count } ?? ""
   }
   
   /// Returns information about a currency by its code.
   static func currency(for code: String) -> Currency? {
      return cache[code]
   }
   
   // Global constants and variables are always computed lazily, in a similar manner to Lazy Stored Properties.
   static fileprivate var cache: [String: Currency] = { () -> [String: Currency] in
      var mapCurrencyCode2Symbols: [String: Set<String>] = [:]
      let currencyCodes = Set(Locale.commonISOCurrencyCodes)
      
      for localeId in Locale.availableIdentifiers {
         let locale = Locale(identifier: localeId)
         guard let currencyCode = locale.currencyCode, let currencySymbol = locale.currencySymbol else {
            continue
         }
         if currencyCode.contains(currencyCode) {
            mapCurrencyCode2Symbols[currencyCode, default: []].insert(currencySymbol)
         }
      }
      
      var mapCurrencyCode2Currency: [String: Currency] = [:]
      for (code, symbols) in mapCurrencyCode2Symbols {
         mapCurrencyCode2Currency[code] = Currency(code: code, symbols: Array(symbols))
      }
      return mapCurrencyCode2Currency
   }()
}
