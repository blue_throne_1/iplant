//
//  PlantInfoModel.swift
//  iPlant
//
//  Created by Pietro Messineo on 17.12.2021.
//

import Foundation
import RxSwift
import RxCocoa

class PlantInfoModel {
    var plantInfo: BehaviorRelay<PlantResponse?> = BehaviorRelay(value: nil)
    var plantErr: BehaviorRelay<State> = BehaviorRelay(value: .waiting)
    
    enum State {
        case loading
        case error
        case success
        case waiting
    }
}
