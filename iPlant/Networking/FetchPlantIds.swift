//
//  FetchPlantIds.swift
//  iPlant
//
//  Created by Pietro Messineo on 20.12.2021.
//

import Foundation
import UIKit

class FetchPlantIds {
    static let shared = FetchPlantIds()
    
    func fetchPlantIds(ids: [Int], completion: @escaping ([PlantResponse]) -> (), errorState: @escaping (Error?) -> () ) {
        //let languagePrefix = Locale.preferredLanguages[0].prefix(2)
        let url = "https://api.plant.id/v2/get_identification_result/multiple"
        
        print("I GOT IDS \(ids.description) OR IDS \(ids)")
        
        let jsonString = """
{
"ids": \(ids),
"plant_language" : "\(UserDefaults.standard.string(forKey: "languageIso") ?? "en")",
"modifiers": ["similar_images","health_all"],
"disease_details": ["common_names", "url", "description"],
"plant_details": ["common_names", "url", "wiki_description", "taxonomy", "wiki_images"]
}
"""
        print("JSON STRING OF IDS \(jsonString)")
        print("FETCH PLANT URL \(url)")
        let urlObj = URL(string: url)
        var request = URLRequest(url: urlObj!)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField:"Content-Type")
        request.setValue("9gKOQB6kIAoLNunnsCq28tzO1FHE14BkMna7ThQZ1wpnwDZ3go", forHTTPHeaderField:"Api-Key")
        request.httpBody = jsonString.data(using: .utf8)
        
        URLSession.shared.dataTask(with: request) {(data, response, error) in
            DispatchQueue.global().async {
                if let error = error {
                    errorState(error)
                }else {
                    do {
                        print("I GOT RESPONSE \(String(data: data!, encoding: .utf8))")
                        let unlockData = try JSONDecoder().decode([PlantResponse].self, from: data!)
                        DispatchQueue.main.async {
                            completion(unlockData)
                        }
                    } catch {
                        print("ERROR FETCHING DATA", error)
                        errorState(error)
                    }
                }
            }
        }.resume()
    }
}
