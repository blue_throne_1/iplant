//
//  FetchPlantInfo.swift
//  iPlant
//
//  Created by Pietro Messineo on 17.12.2021.
//

import Foundation
import UIKit

class FetchPlantInfo {
    static let shared = FetchPlantInfo()
    
    func fetchPlantInfo(plantImage: UIImage, completion: @escaping (PlantResponse) -> (), errorState: @escaping (Error?) -> () ) {
        //let languagePrefix = Locale.preferredLanguages[0].prefix(2)
        let url = "https://api.plant.id/v2/identify"
        
        let imageData = plantImage.jpegData(compressionQuality: 1)!.base64EncodedString()
        
        let jsonString = """
{
"images": ["\(imageData)"],
"plant_language" : "\(UserDefaults.standard.string(forKey: "languageIso") ?? "en")",
"modifiers": ["similar_images","health_all"],
"disease_details": ["common_names", "url", "description"],
"plant_details": ["common_names", "url", "wiki_description", "taxonomy", "wiki_images"]
}
"""
        print("FETCH PLANT URL \(url)")
        let urlObj = URL(string: url)
        var request = URLRequest(url: urlObj!)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField:"Content-Type")
        request.setValue("9gKOQB6kIAoLNunnsCq28tzO1FHE14BkMna7ThQZ1wpnwDZ3go", forHTTPHeaderField:"Api-Key")
        request.httpBody = jsonString.data(using: .utf8)
        
        URLSession.shared.dataTask(with: request) {(data, response, error) in
            DispatchQueue.global().async {
                if let error = error {
                    errorState(error)
                }else {
                    do {
                        let unlockData = try JSONDecoder().decode(PlantResponse.self, from: data!)
                        DispatchQueue.main.async {
                            completion(unlockData)
                        }
                    } catch {
                        print("I GOT RESPONSE ERROR \(String(data: data!, encoding: .utf8))")
                        print("ERROR FETCHING DATA", error)
                        errorState(error)
                    }
                }
            }
        }.resume()
    }
}
