//
//  InAppResponse.swift
//  iPlant
//
//  Created by Pietro Messineo on 21.12.2021.
//

import Foundation

struct InAppResponse: Codable {
    let big_centered_top_title: String?
    let first_feature: String?
    let second_feature: String?
    let third_feature: String?
    let green_button_text: String?
}
