//
//  PlantResponse.swift
//  iPlant
//
//  Created by Pietro Messineo on 17.12.2021.
//

import Foundation

struct PlantResponse: Codable {
    let id: Int
    let images: [Image]?
    let suggestions: [Suggestion]?
    let secret: String?
    let fail_cause: String?
    let is_plant_probability: Double
    let is_plant: Bool
    let health_assessment: Health?
}

//MARK: - Health assessment
struct Health: Codable {
    let is_healthy_probability: Double?
    let diseases: [Disease]?
}

struct Disease: Codable {
    let classification: [String]?
    let name: String?
    let probability: Double?
    let disease_details: DiseaseDetail?
}

struct DiseaseDetail: Codable {
    let common_names: [String]?
    let url: String?
    let description: String?
    let classification: [String]?
    let name: String?
}

// MARK: - Image
struct Image: Codable {
    let fileName: String
    let url: String

    enum CodingKeys: String, CodingKey {
        case fileName = "file_name"
        case url
    }
}

// MARK: - Suggestion
struct Suggestion: Codable {
    let id: Int
    let plantName: String
    let plantDetails: PlantDetails
    let probability: Double
    let confirmed: Bool
    let similarImages: [SimilarImage]

    enum CodingKeys: String, CodingKey {
        case id
        case plantName = "plant_name"
        case plantDetails = "plant_details"
        case probability, confirmed
        case similarImages = "similar_images"
    }
}

// MARK: - PlantDetails
struct PlantDetails: Codable {
    let commonNames: [String]?
    let url: String?
    let wikiDescription: Wiki?
    let taxonomy: Taxonomy
    let wikiImages: [Wiki]?
    let scientificName: String
    let structuredName: StructuredName

    enum CodingKeys: String, CodingKey {
        case commonNames = "common_names"
        case url
        case wikiDescription = "wiki_description"
        case taxonomy
        case wikiImages = "wiki_images"
        case scientificName = "scientific_name"
        case structuredName = "structured_name"
    }
}

// MARK: - StructuredName
struct StructuredName: Codable {
    let genus, species: String?
}

// MARK: - Taxonomy
struct Taxonomy: Codable {
    let taxonomyClass, family, genus, kingdom: String
    let order, phylum: String

    enum CodingKeys: String, CodingKey {
        case taxonomyClass = "class"
        case family, genus, kingdom, order, phylum
    }
}

// MARK: - Wiki
struct Wiki: Codable {
    let value: String
    let citation: String?
    let licenseName: String?
    let licenseURL: String?

    enum CodingKeys: String, CodingKey {
        case value, citation
        case licenseName = "license_name"
        case licenseURL = "license_url"
    }
}

// MARK: - SimilarImage
struct SimilarImage: Codable {
    let id: String
    let similarity: Double
    let url, urlSmall: String

    enum CodingKeys: String, CodingKey {
        case id, similarity, url
        case urlSmall = "url_small"
    }
}
