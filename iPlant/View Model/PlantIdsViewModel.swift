//
//  PlantIdsViewModel.swift
//  iPlant
//
//  Created by Pietro Messineo on 20.12.2021.
//

import Foundation
import RxSwift
import RxCocoa

class PlantIdsViewModel {
    var plantModel = PlantIdsModel()
    
    func dataFetch(ids: [Int]){
        plantModel.plantInfo.accept(nil)
        plantModel.plantErr.accept(.loading)
        
        FetchPlantIds.shared.fetchPlantIds(ids: ids, completion: { data in
            self.plantModel.plantInfo.accept(data)
            self.plantModel.plantErr.accept(.success)
        }, errorState: { error in
            self.plantModel.plantErr.accept(.error)
        })
    }
}
