//
//  PlantInfoViewModel.swift
//  iPlant
//
//  Created by Pietro Messineo on 17.12.2021.
//

import Foundation
import RxSwift
import RxCocoa

class PlantInfoViewModel {
    var plantModel = PlantInfoModel()
    
    func dataFetch(plantImage: UIImage){
        plantModel.plantInfo.accept(nil)
        plantModel.plantErr.accept(.loading)
        
        FetchPlantInfo.shared.fetchPlantInfo(plantImage: plantImage, completion: { data in
            self.plantModel.plantInfo.accept(data)
            self.plantModel.plantErr.accept(.success)
        }, errorState: { error in
            self.plantModel.plantErr.accept(.error)
        })
    }
}
