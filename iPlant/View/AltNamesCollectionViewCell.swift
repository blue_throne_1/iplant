//
//  AltNamesCollectionViewCell.swift
//  iPlant
//
//  Created by Pietro Messineo on 28.12.2021.
//

import UIKit

class AltNamesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var alternativeNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func configure(name: String) {
        alternativeNameLabel.text = name
    }
    
}
