//
//  CarouselCollectionViewCell.swift
//  iPlant
//
//  Created by Pietro Messineo on 28.12.2021.
//

import UIKit
import Kingfisher

class CarouselCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var plantImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        plantImageView.layer.cornerRadius = 20
    }
    
    func configure(image: String) {
        plantImageView.kf.setImage(with: URL(string: image)!)
    }
    
}
