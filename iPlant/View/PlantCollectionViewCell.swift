//
//  PlantCollectionViewCell.swift
//  iPlant
//
//  Created by Pietro Messineo on 17.12.2021.
//

import UIKit
import Kingfisher

class PlantCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var plantImage: UIImageView!
    @IBOutlet weak var plantNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        plantImage.layer.cornerRadius = 15
        self.contentView.layer.cornerRadius = 15
    }
    
    func configure(plant: PlantResponse) {
        if let image = plant.images?.first?.url, let url = URL(string: image) {
            plantImage.kf.setImage(with: url)
        }else {
            plantImage.image = UIImage()
        }
        plantNameLabel.text = plant.suggestions?.first?.plantName
    }
}
