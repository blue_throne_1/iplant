//
//  SettingTableViewCell.swift
//  iPlant
//
//  Created by Pietro Messineo on 17.12.2021.
//

import UIKit

class SettingTableViewCell: UITableViewCell {
    
    @IBOutlet weak var settingTitleLabel: UILabel!
    @IBOutlet weak var languageLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var chevronImageView: UIImageView!
    @IBOutlet weak var settingIconImageView: UIImageView!
    
    var settings = ["Language", "Clear cache", "QON ID", "Terms & Conditions", "Privacy Policy", "Contact us"]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        containerView.layer.cornerRadius = 8
        settingIconImageView.layer.cornerRadius = 6
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(setting: String) {
        if setting == settings[0] {
            settingIconImageView.image = UIImage(systemName: "globe.americas")
            chevronImageView.isHidden = false
            languageLabel.isHidden = false
        }else if setting == settings[1] {
            settingIconImageView.image = UIImage(systemName: "trash")
            chevronImageView.isHidden = true
            languageLabel.isHidden = true
        }else if setting == settings[2] {
            settingIconImageView.image = UIImage(systemName: "key")
            chevronImageView.isHidden = true
            languageLabel.isHidden = true
        }else if setting == settings[3] {
            settingIconImageView.image = UIImage(systemName: "doc")
            chevronImageView.isHidden = false
            languageLabel.isHidden = true
        }else if setting == settings[4] {
            settingIconImageView.image = UIImage(systemName: "lock")
            chevronImageView.isHidden = false
            languageLabel.isHidden = true
        }else if setting == settings[5] {
            settingIconImageView.image = UIImage(systemName: "message")
            chevronImageView.isHidden = false
            languageLabel.isHidden = true
        }
        
        settingTitleLabel.text = setting
        languageLabel.text = UserDefaults.standard.string(forKey: "languageIso") ?? "En"
    }

}
